require_relative 'block'

class BlockExpression < Expression

  @TYPE = 'block_expression'

  def initialize(parent_id, controller, prefix, suffix)
    puts "initializing block expression: parentId = #{parent_id}, prefix = #{prefix}, suffix = #{suffix}..."
    @parent_id = parent_id
    @suffix = suffix
    @controller = controller
    @block = parse_block prefix
    parent = @controller.get_block_by_id @parent_id
    unless parent.equal? nil
      parent.add_child_id @block.get_id
    end
  end

  def parse_block (block_body)
    block_body.strip
    #todo: add block to the controller
    Block.new block_body, @controller, @parent_id
  end

  def get_block
    @block
  end

  def get_suffix
    @suffix
  end

  def getExpressionType
    @TYPE
  end

  def run
    #todo: if the suffix is not nil, find the appropriate value to return
    @block.run
  end

  private :parse_block

end