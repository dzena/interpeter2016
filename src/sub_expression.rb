class SubExpression < Expression
  @TYPE = 'sub_expression'
  def initialize(parent_id, controller, sub_expression, suffix)
    puts 'Initializing a subexpression'
    @sub_expression = sub_expression
    @parent_id = parent_id
    @controller = controller
    @expression_parser = ExpressionParser.new parent_id, controller
    @expression = parse_sub_expression sub_expression
  end

  def parse_sub_expression(sub_expression)
    sub_expression = sub_expression[1..(sub_expression.length - 2)]
    @expression_parser.parse_expression sub_expression
  end

  def get_expression
    @expression
  end

  def run
    @expression.run
  end

  def getExpressionType
    @TYPE
  end
end