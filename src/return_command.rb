require_relative 'command'

class RETURN_COMMAND < COMMAND

  def run
    @expression.run
  end

  def read_command(line)
    puts 'parsing command: ' + line
    expression_value = line[1, line.size-1]
    @expression = @expression_parser.parse_expression(expression_value)
  end

end