require File.expand_path('controller')

class FILE_HANDLER

  def initialize

  end

  def read_file(path_to_file)
    text = open(path_to_file)
    Controller.new(text.read)
  end

  def create_ruby_file(content)

  end

  def execute_ruby_file(path_tofile)

  end

  def run
    ruby_text = read_file(path_to_file)
    ruby_file = create_ruby_file(ruby_text)
    execute_ruby_file(ruby_file)
  end

end