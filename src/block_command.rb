require_relative 'command'

class BLOCK_COMMAND < COMMAND

  def run
    temp = @expression.run
    if @property_name != nil
      @property_value = temp
    end
  end

  def read_command(line)
    puts 'parsing command: ' + line
    name_value, expression_value = split_command(line[0, line.size-2], '=')
    @expression = @expression_parser.parse_expression(expression_value)
    @property_name = name_value
    return "#{name_value} = #{@expression}"
    rescue NoNameException
    return "#{line[1, line.size-2]}"
  end

end