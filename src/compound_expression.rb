require_relative 'expression_parser'
require_relative 'block'

class CompoundExpression < Expression
  @TYPE = 'compound_expression'
  def initialize(parent_id, controller, compounds)
    puts 'initializing new compound expression'
    @compounds = compounds
    @parent_id = parent_id
    @controller = controller
    @expressions = Array.new
    parse_compound_expressions compounds

  end

  def parse_compound_expressions (compounds)
    expression_parser = ExpressionParser.new @parent_id, @controller
    compounds.each do |compound|
      puts "parsing compound #{compound.get_prefix}"

      expression = expression_parser.parse_expression compound.get_prefix
      @expressions<<expression
    end

  end

  def run
    puts 'Running compound expression ... '
    resulting_block = Block.new '{}', @controller, @parent_id
    #if @expressions[0].is_a?(BlockExpression)

      @expressions.each do |expression|
        temp_block = expression.run
        temp_block.get_property_list.each do |name, value|
          resulting_block.add_property name, value
        end
      end

      #1. check that all compounds are either block expressions or names of
      # some properties of type Block. We can only concat things of the same type
      #2. Create a new 'result_block'
      #3. Iterate through expressions
      #3.1 in each iteration, run the current block an concat the property list with the 'result_block'
    #elsif @compounds.is_a?(StringLiteralExpression)
      #1. check that all compounds are string literal expressions or name expressions.
      #   We can concat a string literal with the value of a name (only if the type of the name is 'primitive' i.e. is not a bloc
      #2. create a resulting literal
    #end

    resulting_block
  end

  def add_block_expressions (left_expression, right_expression, parent_id) # not used
    resulting_block = Block.new '{}', @controller, parent_id
    left_prop_list = left_expression.run
    left_prop_list.each do |name , value|
      resulting_block.add_property name, value
    end

    right_prop_list = right_expression.run
    right_prop_list.each do |name, value|
      resulting_block.add_property name, value
    end

    resulting_block
  end

  def getCompounds
    @compounds
  end

  def getExpressionType
    @TYPE
  end

  def getBindingCompound

  end
end