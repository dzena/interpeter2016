require_relative '../../src/block'
require_relative '../../src/controller'
require 'test/unit'

class BlockTest < Test::Unit::TestCase

  def test_block_parse
    controller = Controller.new '{ }'
    block_val = '{ x = \'3\'; y = \'4\';}'
    block = Block.new block_val, controller, nil
    puts 'commands: ' + block.get_commands.size.to_s
    block.run
    puts 'block is created successfully'
    block.get_property_list.each do |prop, val|
      puts "prop: #{prop} => val: #{val}"
    end
    puts "checking assertion.. #{block.class}"

    assert_instance_of(Block, block, 'Expecting block to be an instance of Block class')
  end

  def test_multiple_line_block
    controller = Controller.new '{ }'
    block_val = '{ x = \'5\';
 y = \'6\';}'
    block = Block.new block_val, controller, nil
    puts 'commands: ' + block.get_commands.size.to_s
    block.run
    puts 'block is created successfully'
    block.get_property_list.each do |prop, val|
      puts "prop: #{prop} => val: #{val}"
    end
    puts "checking assertion.. #{block.class}"

    assert_instance_of(Block, block, 'Expecting block to be an instance of Block class')
  end

  def test_nested_blocks
    controller = Controller.new '{ }'
    block_val = '{ x = \'5\'; y = \'6\'; z = {a =\'7\'; b =\'8\';};}'
    block = Block.new block_val, controller, controller.get_root_block.get_id
    puts 'num of commands: ' + block.get_commands.size.to_s
    block.run
    block.get_property_list.each do |prop, val|
      puts "prop: #{prop} => val: #{val}"

    end
    puts "checking assertion.. #{block.class}"
    assert_instance_of(Block, block, 'Expecting block to be an instance of Block class')

    # controller.get_block_tree.each do |prop, val|
    #   puts "prop: #{prop} => "
    #   val.get_property_list.each do |prop_name, prop_val|
    #     puts "prop: #{prop_name} => val: #{prop_val}"
    #   end
    # end

    puts " block size: #{controller.get_block_tree.size} "

    controller.get_block_tree.each do |id, b|
      puts "#{id} => #{b.get_unparsed_body}, parentId: #{b.get_parent_id} "
    end

  end


  def test_simple_name_reference
    controller = Controller.new '{ }'
    block_val = '{ x = \'5\'; y = \'6\'; z = { a = *x; b = *y ;};}'
    block = Block.new block_val, controller, controller.get_root_block.get_id
    puts 'num of commands: ' + block.get_commands.size.to_s
    block.run
    block.get_property_list.each do |prop, val|
      puts "prop: #{prop} => val: #{val}"

    end
    puts "checking assertion.. #{block.class}"
    assert_instance_of(Block, block, 'Expecting block to be an instance of Block class')

    # controller.get_block_tree.each do |prop, val|
    #   puts "prop: #{prop} => "
    #   val.get_property_list.each do |prop_name, prop_val|
    #     puts "prop: #{prop_name} => val: #{prop_val}"
    #   end
    # end

    puts " block size: #{controller.get_block_tree.size} "

    controller.get_block_tree.each do |id, b|
      puts "#{id} => #{b.get_unparsed_body}, parent_id: #{b.get_parent_id} "
    end

  end

end