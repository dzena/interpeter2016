require_relative '../../src/expression_parser'
require_relative '../../src/controller'
require 'test/unit'

class ExpressionParserTest < Test::Unit::TestCase

  def test_parse_string_literal_expression
    string_literal_expression = '\'expression_body\''
    c = Controller.new '{ }'
    expression_parser = ExpressionParser.new nil, c
    expression = expression_parser.parse_expression string_literal_expression
    assert_equal(string_literal_expression, expression.get_prefix)
    assert_instance_of(StringLiteralExpression, expression)
  end

  def test_parse_block_expression
    block_exp_body = '{ x = \'10\'; }'
    c = Controller.new '{ }'
    expression_parser = ExpressionParser.new  nil, c
    expression = expression_parser.parse_expression block_exp_body
    assert_instance_of(BlockExpression, expression)
  end

  def test_compound_expression
    comp_exp_body = '{ x = \'10\';} + {y = \'5\'; x = \'3\';}'
    c = Controller.new '{ }'
    expression_parser = ExpressionParser.new nil, c
    expression = expression_parser.parse_expression comp_exp_body
    assert_instance_of(CompoundExpression, expression)

    resulting_block = expression.run
    resulting_block.get_property_list.each do |name, val|
      puts "#{name} => #{val}"
    end
  end

  def test_compound_sub_expression
    comp_exp_body = '{ x = \'10\';} + ( {y = \'5\'; x = \'3\';} + { z = \'11\';} )'
    c = Controller.new '{ }'
    expression_parser = ExpressionParser.new nil, c
    expression = expression_parser.parse_expression comp_exp_body
    assert_instance_of(CompoundExpression, expression)

    resulting_block = expression.run
    resulting_block.get_property_list.each do |name, val|
      puts "#{name} => #{val}"
    end
  end

  def test_two_sub_expressions
    comp_exp_body = '({ x = \'10\';} + { y = \'5\'; z = \'4\';} ) + ( {y = \'1\'; x = \'3\';} + { z = \'11\';} )'
    c = Controller.new '{ }'
    expression_parser = ExpressionParser.new nil, c
    expression = expression_parser.parse_expression comp_exp_body
    assert_instance_of(CompoundExpression, expression)

    resulting_block = expression.run
    resulting_block.get_property_list.each do |name, val|
      puts "#{name} => #{val}"
    end

    puts " block size: #{c.get_block_tree.size} "

    c.get_block_tree.each do |id, block|
      puts "#{id} => #{block.get_unparsed_body}, parentId: #{block.get_parent_id} "
    end

  end
end