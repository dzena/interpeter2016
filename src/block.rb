require_relative 'command'
require_relative 'interpreter_exception'

class Block

  def initialize(block_body, controller, parent_id)
    puts 'Initializing block: ' + block_body
    @unparsed_body = block_body
    @commands = Array.new # for storing the commands in the body of the block
    @property_list = Hash.new
    @names = Array.new # for storing the names defined inside the block
    @parent_id = parent_id
    @children_id = Array.new
    @controller = controller
    @id = controller.generate_next_block_id
    controller.add_block @id, self
    parse_block(block_body)
  end

  def to_ruby
    ruby = ''
    @commands.each {|command| ruby = ruby + command + '\n' }
    ruby
  end

  def run
    puts 'Running block..'
    @commands.each do |command|
      #todo: check if it is a return command. in that case return a value
      command.run
      update_property_list command
    end

    self
  end

  def update_property_list (command)
    name = command.get_property_name
    if name != nil
      value = command.get_property_value
      add_property name.strip, value
    end
  end

  # Returns the (possibly empty) list  of the names defined inside the block
  def get_names
    @names
  end

  def get_unparsed_body
    @unparsed_body
  end

  def add_child_id (child_id)
    @children_id << child_id
  end

  def get_children_id
    @children_id
  end

  def add_property(name, value)
    @property_list.store name, value
  end

  def get_property_value(name)
    @property_list.fetch(name)
  end

  def get_property_list
    @property_list
  end

  def get_parent_id
    @parent_id
  end

  def get_child_id
    @children_id
  end

  def set_child_id (child_id)
    @children_id = child_id
  end

  def get_commands
    @commands
  end

  def parse_block(line)
    puts 'parsing block: ' + line

    line = line.strip

    if line.start_with?('{') && line.end_with?('}')
      block_body = line[1..(line.length - 2)].strip #skip the curly brackets
      @str_block = block_body
      i = 0
      block_length = block_body.length

      while i < block_length

        while block_body[i].eql?' ' or block_body[i].eql?"\n" #skip spaces and new lines
          i = i + 1
        end

        str_command = extract_next_command(block_body[i..block_length-1])
        command =  COMMAND.new @id, str_command, @controller
        puts 'adding command to block'
        @commands<<command

        i = i + str_command.length

        if i > block_length
          raise INTERPRETER_EXCEPTION , 'Error parsing the block'
        end

        # skip spaces. This method should take care of skipping the spaces between commands
        while (block_body[i].eql?(' ') ) && ( i < block_length )
          i = i + 1
        end

      end
    else
      raise 'invalid block format'
    end

    block_body
  end

  def extract_next_command(str_block_body)

    puts 'extracting next command from :' + str_block_body
    # remove spaces in the left
    str_block_body = str_block_body.lstrip

    if str_block_body.start_with?('[')
      next_command = extract_next_guard(str_block_body)
    else
      next_command = extract_next_semi_column_separated(str_block_body)
    end

    next_command
  end

  def extract_next_guard(str_block_body)

    puts 'extracting next guard command from : ' + str_block_body
    i = str_block_body[/\A */].size #skip the spaces in the left

    if str_block_body[i].eql?('[')
      c = 1
      begin
        i = i + 1
        if str_block_body[i].eql?('[')
          c = c + 1
        end

        if str_block_body[i].eql?(']')
          c = c - 1
        end

        if i >=  str_block_body.length
          raise INTERPRETER_EXCEPTION , 'Error while trying to extract the next guard command of the block: missing closing bracket'
        end
      end until c == 0
    end
    str_block_body[0..i]
  end

  def extract_next_semi_column_separated(str_block_body)
    puts 'extracting next semicolon separated command from : ' + str_block_body
    i = str_block_body[/\A */].size #skip the spaces in the left
    while str_block_body[i] != ';'
      #skip blocks
      if str_block_body[i] == '{'
        c = 1
        begin
          i = i + 1
          if str_block_body[i] == '{'
            c = c + 1
          end
          if str_block_body[i] == '}'
            c = c - 1
          end
        end until c == 0
      end

      #skip expressions
      if str_block_body[i] == '('
        c = 1
        begin
          i = i + 1
          if str_block_body[i] == '('
            c = c + 1
          end
          if str_block_body[i] == ')'
            c = c - 1
          end
        end until c == 0
      end

      i = i + 1
    end

    puts 'Found command: ' + str_block_body[0..i]
    str_block_body[0..i]
  end

  def get_str_block
    @str_block
  end

  def get_id
    @id
  end


  private :extract_next_command, :extract_next_semi_column_separated, :extract_next_guard, :update_property_list
  
end