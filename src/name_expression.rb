class NameExpression < Expression
  @TYPE = 'name'

  def initialize(parent_id, controller, name, suffix)
    puts "initializing name expression #{name} , suffix: #{suffix}"
    @name = name.strip
    @suffix = suffix
    @parent_id = parent_id
    @controller = controller
  end

  def get_name_prefix
    @name
  end

  def get_name_suffix
    @suffix
  end

  def run
    puts "running name expression #{@name}, current_id = #{@parent_id}"
    @controller.get_by_block_id_property_name @parent_id, @name
    # 2.1  take care of the possible suffix.
  end

  def getExpressionType
    @TYPE
  end
end