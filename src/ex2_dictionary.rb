require 'singleton'

# only one dictionary is needed
class EX2_DICTIONARY
  include Singleton

  def initialize
    @dictionary = {'echo' => 'print'}
  end

  # return the ruby translation for valid ex2 commands, if the submitted word does not exist, an empty string is returned
  def translate_to_ruby(word)
    @dictionary.fetch(word, '')
  end
end