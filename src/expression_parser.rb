require_relative 'expression'
require_relative 'block_expression'
require_relative 'compound_expression'
require_relative 'string_literal_expression'
require_relative 'name_expression'
require_relative 'sub_expression'
require_relative 'interpreter_exception'

class ExpressionParser

  def initialize(parent_id, controller)
    @parent_id = parent_id
    @controller = controller
  end

  def parse_expression (value)
    @value = value

    components = extract_components value
    if components.count > 1
      #construct a new compound expression
      compound_expression = CompoundExpression.new(@parent_id, @controller, components)
      expression = compound_expression
    elsif components.count == 1
      component = components[0]
      prefix = component.get_prefix
      suffix = component.get_suffix

      if prefix.start_with?('\'')
        expression = StringLiteralExpression.new(@parent_id, @controller, prefix, suffix)
      elsif prefix.start_with?('{')
        expression = BlockExpression.new(@parent_id, @controller, prefix, suffix)
      elsif prefix.start_with?('(')
        expression = SubExpression.new(@parent_id, @controller, prefix, suffix)
      else
        expression = NameExpression.new(@parent_id, @controller, prefix, suffix)
      end

      #todo: put restrictions on the symbols allowed in expressions
    else
      #todo: write a new exception to properly indicate the problem
      raise 'incorrect syntax'
    end
    expression

  end

  def extract_components(expr_value)
    components = Array.new
    expr_size = expr_value.size
    i = 0
    begin
      while i < expr_size and expr_value[i].eql?' '
        i = i + 1
      end
      prefix = parse_expression_prefix(expr_value[i .. expr_size-1])
      prefix_length = prefix.size
      suffix = ''
      suffix_length = 0
      if i + prefix_length < expr_value.size
        #TODO: either skip spaces between the end of expression and the suffix or dont allow them in the language
        if expr_value[prefix_length] == '.'
          suffix = parse_expression_suffix(expr_value[prefix_length..expr_size-1])
          suffix_length = suffix.size
        end
      end
      i = i + prefix_length + suffix_length

      #skip spaces
      if i < expr_size
        while i < expr_size && expr_value[i] == ' ' do
          i = i + 1
        end

        if i < expr_size - 1 and expr_value[i].eql?'+'
          i = i + 1 #skip '+'
        elsif i < expr_size - 1 and !expr_value[i].eql?'+'
          puts expr_value  + "[#{i}]" + "expression size = #{expr_size}"
          raise 'incorrect syntax'

        end
      end

      components<<Expression.new(prefix, suffix)

    end until i>= expr_size

    return components
  end

  def parse_expression_prefix (value)

    i = 0
    compound_found = false

    while i < value.size and !value[i].eql?'.' and not compound_found
      c = 0
      raise INTERPRETER_EXCEPTION, 'Not valid expression' if i >= value.length
      if value[i].eql?'('
        c = c + 1
        while c > 0
          i = i + 1
          raise INTERPRETER_EXCEPTION, 'Not valid expression' if i >= value.length
          if value[i].eql?'('
            c = c + 1
          elsif value[i].eql?')'
            c = c - 1
          end
        end

        compound_found = true

      elsif value[i].eql?'{'
        c = c + 1
        while c > 0
          i = i + 1
          raise INTERPRETER_EXCEPTION, 'Not valid expression' if i >= value.length
          if value[i].eql?'{'
            c = c + 1
          elsif value[i].eql?'}'
            c = c - 1
          end
        end

        compound_found = true

      end

      if compound_found
        while i < value.size and value[i].eql?' ' #skip spaces after finding a compound
          i = i + 1
        end
      else
        i = i + 1
      end

    end
    expression_prefix = value[0..i]

    puts 'found expression: ' + expression_prefix

    return expression_prefix
  end

  def parse_expression_suffix(value)
    i = 0
    suffix = ''
    if value[i] == '.'
      begin
        i = i + 1
      end until value[i] == ' ' || value[i] == '+'
      suffix = value[0..i]
    end
    return suffix
  end

  # def test_method
  #   puts 'welcome to ruby'
  #   value = '(st long(nested bracket) str(as)ing ) asdf'
  #   c = 0
  #   i = 0
  #   if value[0] == '('
  #     c = c + 1
  #
  #     while c > 0 do
  #       i = i + 1
  #       raise 'Not valid expression' if i >= value.length
  #       if value[i] == ')'
  #         c = c - 1
  #       end
  #
  #       if value[i] == '('
  #         c = c + 1
  #       end
  #     end
  #   else
  #     puts 'Expression must start with \'(\' '
  #   end
  #   puts value[0 .. i]
  # end


end