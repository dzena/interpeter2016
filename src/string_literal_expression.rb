class StringLiteralExpression < Expression

  @TYPE = 'string_literal'
  def initialize(parent_id, controller, prefix, suffix)
    #todo: check whether prefix is enclosed in brackets and quotes
    #todo: check whether the suffix starts with '.'
    @prefix = prefix
    @suffix = suffix
    @parent_id = parent_id
    @controller = controller

  end

  def run
    if @suffix.eql?(nil) or @suffix.eql?''
      @prefix.tr('\'', '')
    else
      #todo: find the value represented by suffix and return it.
      #todo: if it does make sense, use the value of the string literal as a property name.
    end
  end

  def get_prefix
    @prefix
  end

  def get_suffix
    @suffix
  end

  def to_ruby
    #todo: how to include the suffix
    ruby_value = @prefix.tr('()', '')
    return ruby_value
  end

  def getExpressionType
    @TYPE
  end
end