require_relative 'interpreter_exception'
require_relative 'no_name_exception'
require_relative 'expression_parser'
require_relative 'block'
require_relative 'guard'
require_relative 'return_command'
require_relative 'block_command'
require_relative 'guard_command'

class COMMAND

  def initialize(parent_id, line, controller)
    puts "initializing command: #{line}"
    @parent_id = parent_id
    @controller = controller
    @expression_parser = ExpressionParser.new(@parent_id, controller)
    @guard_parser = GUARD.new(@parent_id, @controller)
    @command_body = read_command line
    @property_name = nil
    @property_value = nil
    @expression = nil
    @guard = nil
  end

  def get_property_name
    @property_name
  end

  def get_property_value
    @property_value
  end

  def read_command(line)
    puts 'parsing command: ' + line

    if line.start_with?('^')
      if line.end_with?(';')
        return_command = RETURN_COMMAND.new(@parent_id, line, @controller).read_command(line)
        return "return #{return_command}"
      else
        raise INTERPRETER_EXCEPTION, "Return command needs to end with #{;}!"
      end
    else
      if line.start_with?('[')
        puts "guard command found: #{line}"
        if line.end_with?(']')
          guard_command = GUARD_COMMAND.new(@parent_id, line, @controller).read_command(line)
          return "#{guard_command}"
        else
          raise INTERPRETER_EXCEPTION, 'Guard command needs to start and end with [, ]!'
        end
      else
        if line.end_with?(';')
          block_command = BLOCK_COMMAND.new(@parent_id, line, @controller).read_command(line)
          return "#{block_command}"
        else
          raise INTERPRETER_EXCEPTION, "Block command needs to end with #{;}!"
        end
      end
    end
  end

  def split_command(line, symbol)
    i = 0
    found = false
    while i < line.size and found == false
      if line[i].eql?symbol
        found = true
        if i > 0 and i < line.size-1
          line_before = line[0..i-1]
          line_after = line[i+1..line.size-1]
          puts "Splitting the command #{line} at #{i} to: #{line_before} , #{line_after}"
          return line_before, line_after
        end
      end
      i = i+1
    end
    raise NoNameException, "No proper command, #{symbol} not found!"
  end


end