require_relative 'expression_parser'

class GUARD

  def initialize(parent_id, controller)
    @parent_id = parent_id
    @controller = controller
    @expression_parser = ExpressionParser.new(parent_id, controller)
  end

  def read_guard(line)
    line.strip
    if line.include? "="
      expressions = line.split('=')
      test = @expression_parser.parse_expression(expressions[0])
      test_against = @expression_parser.parse_expression(expressions[1])

      if test == test_against
        true
      else
        false
      end

    elsif line.include? "#"
      expressions = line.split('#')
      test = @expression_parser.parse_expression(expressions[0])
      test_against = @expression_parser.parse_expression(expressions[1])

      if test != test_against
        true
      else
        false
      end

    end
  end
end