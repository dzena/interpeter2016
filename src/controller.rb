require_relative 'block'

class Controller
  def initialize(program_body)
    puts 'Initializing controller'
    @bid = 0
    @block_tree = Hash.new
    @root_block = Block.new(program_body, self, nil )

  end

  def get_root_block
    @root_block
  end

  # adds a new block in the map. Overrides the possibly existing one
  def add_block (id, block)
    @bid = @bid + 1;
    @block_tree.store id, block
  end

  # this method generates the id of the next block to be added
  def generate_next_block_id
    'block_' + @bid.to_s
  end

  # finds the block corresponding to the given id on the map
  def get_block_by_id(id)
    @block_tree.fetch id, nil #the second parameter is the default value
  end

  #gets the parent of the block with the given id
  def get_parent_by_child_id(id)
    block = @block_tree.fetch id
    parent_id = block.get_parent_id
    @block_tree.fetch parent_id
  end

  #finds the property value based in the given block id
  # and the name of the property. name be preceded by an
  # arbitrary number of '*'.
  def get_by_block_id_property_name (id, name)
    current = get_block_by_id id
    if name.start_with?'*'
      parent = get_block_by_id current.get_parent_id
      return get_by_block_id_property_name parent.get_id, name[1..name.length-1].strip
    else
      current.get_property_value name.strip
    end

    #todo: if name contains '.', then split, and starting from the first get the property lists
  end

  def get_block_tree
    @block_tree
  end
end