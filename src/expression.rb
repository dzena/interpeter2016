class Expression

  def initialize(prefix, suffix)
    @prefix = prefix
    @suffix = suffix

  end

  def get_prefix
    @prefix
  end

  def get_suffix
    @suffix
  end

  def run
    raise INTERPRETER_EXCEPTION, 'Unimplemented exception'
  end
end