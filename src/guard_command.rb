require_relative 'command'

class GUARD_COMMAND < COMMAND

  def initialize(parent_id, line, controller)
    puts "initializing command: #{line}"
    @parent_id = parent_id
    @controller = controller
    @guard_parser = GUARD.new(@parent_id, @controller)
    @command_body = read_command line
    @guard = nil
    @command_list = Array.new # containing the commands
    @property_list = Hash.new



  end

  def run
    guard_value, command_value = split_command(line[1,line.size-2], ':')
    @guard = @guard_parser.read_guard(guard_value) #todo @guard_parser.run?
    #todo check result of guard
    if command_value != ''
      block_parser = Block.new(command_value, controller, parent_id)
      @command_list = block_parser.get_commands
      block_parser.run
    end
  end

  def read_command(line)
    puts 'parsing command: ' + line
    guard_value, command_value = split_command(line[1,line.size-2], ':')
    parsed_guard = @guard_parser.read_guard(guard_value)
    # TODO: am I allowed to use Block.new or would I mess up the hierarchy?
    if command_value != ''
    #block_parser = Block.new(command_value, block)
    # TODO: would the recursion return everything where it should be?
    end
#    return "if #{parsed_guard} #{parsed_command}"
  end

end