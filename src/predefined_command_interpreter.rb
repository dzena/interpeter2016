require File.expand_path('ex2_dictionary')

class PREDEFINED_COMMAND_INTERPRETER

  def initialize
    @dictionary = EX2_DICTIONARY.instance
  end

  # controls whether expression is a sign that has the same meaning in most computer languages
  def is_universal_expression?(word)
    universal_expression = %w(+, -, *, /, %, <, >)
    i = 0
    while i < universal_expression.size
      if word == universal_expression[i]
        return true
      end
      i = i+1
    end
    return false
  end


  # return the ruby translation for valid ex2 commands, if the submitted word does not exist, an empty string is returned
  def get_rb_translation(word)
    @dictionary.translate_to_ruby(word)
  end

end